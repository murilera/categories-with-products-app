const express = require('express')
const {
  getCategories,
  getCategory,
  createCategory,
  updateCategory,
  deleteCategory,
  exportCategory,
  importCategory
} = require('../controllers/categories.controller')

const router = express.Router()

router.route('/').get(getCategories).post(createCategory)
router.route('/:id').get(getCategory).put(updateCategory).delete(deleteCategory)
router.route('/:id/export').get(exportCategory)
router.route('/:id/import').post(importCategory)

module.exports = router
