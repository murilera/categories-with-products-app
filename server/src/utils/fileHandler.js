const fs = require('fs')

const readFile = (
  callback,
  returnJson = false,
  filePath,
  encoding = 'utf8'
) => {
  fs.readFile(filePath, encoding, (err, data) => {
    if (err) {
      throw err
    }

    callback(returnJson ? JSON.parse(data) : data)
  })
}

const writeFile = (filePath, fileData, encoding = 'utf8') => {
  fs.writeFile(filePath, fileData, encoding, function (err) {
    if (err) {
      console.log('An error occured while writing JSON Object to File.')
      return console.log(err)
    }
  })
}

const deleteFile = (filePath, res) => {
  const file = fs.createReadStream(filePath)
  file.on('end', function () {
    fs.unlink(filePath, () => {})
  })
  file.pipe(res)
}

module.exports = {
  readFile,
  writeFile,
  deleteFile
}
