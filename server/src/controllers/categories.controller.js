const prisma = require('../config/prisma')
const asyncHandler = require('../middlewares/asyncHandler.middleware')
const ErrorResponse = require('../utils/errorResponse')
const fs = require('fs')
const { writeFile, deleteFile } = require('../utils/fileHandler')

exports.getCategories = asyncHandler(async (req, res, next) => {
  // pagination
  const page = parseInt(req.query.page, 10) || 1
  const limit = parseInt(req.query.limit, 10) || 25
  const startIndex = (page - 1) * limit
  const endIndex = page * limit
  const total = await prisma.category.count()

  const categories = await prisma.category.findMany({
    skip: startIndex,
    take: limit,
    include: {
      products: true
    }
  })

  const pagination = {}
  if (endIndex < total) {
    pagination.next = {
      page: page + 1,
      limit
    }
  }
  if (startIndex > 0) {
    pagination.prev = {
      page: page - 1,
      limit
    }
  }

  const data = {
    status: 'success',
    count: categories.length,
    pagination,
    data: categories
  }

  res.status(200).json(data)
})

exports.getCategory = asyncHandler(async (req, res, next) => {
  const { id } = req.params

  const category = await prisma.category.findUnique({
    where: { id },
    include: {
      products: true
    }
  })

  if (!category) {
    return next(new ErrorResponse(`Category not found with id of ${id}`, 404))
  }

  const data = {
    success: true,
    data: category
  }

  res.status(200).json(data)
})

exports.createCategory = asyncHandler(async (req, res, next) => {
  const { title } = req.body

  const category = await prisma.category.create({
    data: {
      title
    }
  })

  const data = {
    success: true,
    data: category
  }

  res.status(201).json(data)
})

exports.updateCategory = asyncHandler(async (req, res, next) => {
  const { id } = req.params
  const { title } = req.body
  const category = await prisma.category.update({
    where: { id },
    data: {
      title
    }
  })

  if (!category) {
    return next(new ErrorResponse(`Category not found with id of ${id}`, 404))
  }

  const data = {
    success: true,
    data: category
  }

  res.status(200).json(data)
})

exports.deleteCategory = asyncHandler(async (req, res, next) => {
  const { id } = req.params
  const category = await prisma.category.delete({
    where: { id }
  })

  if (!category) {
    return next(new ErrorResponse(`Category not found with id of ${id}`, 404))
  }

  const data = {
    success: true,
    data: {}
  }

  res.status(200).json(data)
})

exports.exportCategory = asyncHandler(async (req, res, next) => {
  const { id } = req.params

  const category = await prisma.category.findUnique({
    where: { id },
    select: {
      products: true
    }
  })

  if (!category) {
    return next(new ErrorResponse(`Category not found with id of ${id}`, 404))
  }

  const data = JSON.stringify(category)
  const filePath = `${id}.json`

  writeFile(filePath, data, 'utf8', function (err) {
    if (err) {
      console.log('An error occured while writing JSON Object to File.')
      return console.log(err)
    }
  })

  deleteFile(filePath, res)
  res.status(200)
  res.attachment(filePath)
})

exports.importCategory = asyncHandler(async (req, res, next) => {
  const { id } = req.params
  const importData = req.body
  const category = await prisma.product.createMany({
    data: importData
  })

  if (!category) {
    return next(new ErrorResponse(`Category not found with id of ${id}`, 404))
  }

  const data = {
    success: true,
    data: category
  }

  res.status(200).json(data)
})
