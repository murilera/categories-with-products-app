const express = require('express')
const dotenv = require('dotenv')
const morgan = require('morgan')
const errorHandler = require('./middlewares/errorHandler.middleware')

const categories = require('./routes/categories.routes')

// load envs
const env = dotenv.config()
if (env.error) {
  throw env.error
}

const app = express()

// body parser
app.use(express.json())

// dev logging middlware
if (process.env.NODE_ENV === 'development') app.use(morgan('dev'))

// define routes
app.use('/api/v1/categories', categories)

app.use(errorHandler)

const PORT = process.env.PORT || 5000
const server = app.listen(
  PORT,
  console.log(`Server running in ${process.env.NODE_ENV} mode on port ${PORT}`)
)

// handle unhandled promise rejections
process.on('unhandledRejection', (err, promise) => {
  console.log(`Error: ${err.message}`)
  server.close(() => process.exit(1))
})
