const { PrismaClient } = require('@prisma/client')
const prisma = new PrismaClient()

async function importData() {
  try {
    const cellPhones = await prisma.category.create({
      data: {
        title: 'Cell Phones & Accessories',
        products: {
          create: {
            title: 'Samsung Galaxy S3'
          }
        }
      }
    })

    const musicalInstruments = await prisma.category.create({
      data: {
        title: 'Musical Instruments',
        products: {
          create: {
            title: 'Fender Guitar'
          }
        }
      }
    })
    console.log({
      status: 'success',
      data: 'Data inserted'
    })
  } catch (error) {
    console.error(error)
  }
}

async function deleteData() {
  try {
    await prisma.category.deleteMany()
    await prisma.product.deleteMany()

    console.log({
      status: 'success',
      data: 'Data deleted'
    })
  } catch (error) {
    console.error(error)
  }
}

if (process.argv[2] === '-i') {
  importData()
} else if (process.argv[2] === '-d') {
  deleteData()
}
