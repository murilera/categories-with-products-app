const ErrorResponse = require('../utils/errorResponse')

const errorHandler = (err, req, res, next) => {
  console.log(err)
  let error = { ...err }

  if (error.statusCode) error.message = err.message
  else error.message = err.meta

  // log to console for dev
  console.log({ code: error.statusCode || error.code, message: error.message })

  res.status(error.statusCode || 500).json({
    success: false,
    error: error.message || 'Server Error'
  })
}

module.exports = errorHandler
