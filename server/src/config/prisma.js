const { PrismaClient } = require('@prisma/client')

let prisma

if (process.env.NODE_ENV === 'development') {
  prisma = new PrismaClient({
    log: ['query']
  })
} else {
  prisma = new PrismaClient()
}

module.exports = prisma
