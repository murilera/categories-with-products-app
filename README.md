# categories-with-products-app

Growth Hackers - Desafio de Produto - Fullstack

Neste desafio você precisa criar uma API com CRUD e exportação/importação.
A API deve exibir para o usuário todas as fontes (categorias) de produtos disponíveis.
Os acessos a esses produtos devem estar disponíveis em um padrão típico de CRUD,
permitindo o cadastro de novos produtos. A listagem de produtos pode ser exportada e
importada dentro de uma categoria diferente.

## backend

- nodejs
- express
- prisma

## frontend (to do)

- reactjs
- tailwindcss

## database

- mongodb (atlas)

## docs

- insomnia

```
cd docs
npx serve
```

## setup && run server

create you .env
replace DATABASE_URL with your mongodb atlas connection
then

```
cd server
npm install
npm run dev
```

## docker build

```
cd server
docker build -t categories-with-products .
docker run -d categories-with-products
```

get docker ip address then make requests to the endpoints
